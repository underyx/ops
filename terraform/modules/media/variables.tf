variable "plex_claim_token" {
  type        = string
  sensitive = true
  description = "Token from https://www.plex.tv/claim/"
}
