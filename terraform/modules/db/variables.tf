variable "postgres_password" {
  type        = string
  sensitive = true
  description = "Postgres database password"
}
